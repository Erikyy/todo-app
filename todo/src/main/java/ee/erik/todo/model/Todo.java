package ee.erik.todo.model;

/**
 * This class is Todo entity
 * 
 * It has id, name and isDone fields
 * 
 * NOTE: id is used as index in arraylist 
 *
 */
public class Todo {
    
    private int id; 
    private String name;
    private boolean isDone;

    public Todo() {}

    public Todo(int id, String name, boolean isDone) {
        this.id = id;
        this.name = name;
        this.isDone = isDone;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isDone() {
        return isDone;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setDone(boolean isDone) {
        this.isDone = isDone;
    }
}

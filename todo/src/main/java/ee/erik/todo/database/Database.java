package ee.erik.todo.database;

import java.util.ArrayList;

import ee.erik.todo.model.Todo;

/**
 * Basic database with arraylist
 */
public class Database {
    
    private ArrayList<Todo> db;

    public Database() {
        db = new ArrayList<>();
    }
    /**
     * 
     * @return simple database arraylist
     */
    public ArrayList<Todo> getDb() {
        return db;
    }

    /**
     * Adds a todo
     * 
     * @param todo
     */
    public void addItem(Todo todo) {
        db.add(todo);
    }

    /**
     * Removes a todo
     * 
     * @param todo
     */
    public void removeItem(Todo todo) {
        db.remove(todo.getId());
    }

    /**
     * Takes existing todo and sets that todo
     * 
     * @param todo
     */
    public void setItem(Todo todo) {
        
        db.set(todo.getId(), todo);
    }

}

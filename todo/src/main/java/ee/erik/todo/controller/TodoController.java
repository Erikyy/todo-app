package ee.erik.todo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ee.erik.todo.database.Database;
import ee.erik.todo.model.Todo;

@RestController
@RequestMapping("/api/v1")
public class TodoController {
    Database database = new Database();

    @GetMapping("/todos")
    public List<Todo> todos() {
        return database.getDb();
    }

    // Usually methods below should return something like added a todo or removed or some todo is set
    @PostMapping("/add")
    public void addTodo(@RequestParam("id") Integer id, @RequestParam("name") String name, @RequestParam("isdone") boolean isDone) {
        Todo todo = new Todo(id, name, isDone);
        database.addItem(todo);
    }

    @PostMapping("/set")
    public void setTodo(@RequestParam("id") Integer id, @RequestParam("isdone") boolean isDone) {
        Todo todo = database.getDb().get(id);
        todo.setDone(isDone);
        database.setItem(todo);
    }

    @PostMapping("/remove")
    public void removeTodo(@RequestParam("id") Integer id) {
        Todo todo = database.getDb().get(id);
        database.removeItem(todo);
    }
}

# Todo-App

Uses spring boot and and supported java version is 11 and up.

To run with command line use ```./gradlew bootRun```


These are some examples on how to use this api.

Couple flaws with this application are that todo id can be used or added again accidentally as this application doesn't autogenerate ids so by removing todo it may delete or set wrong todo, but with databases this is not a problem

Api endpoints:

## Todos 
todos - http://localhost:8080/api/v1/todos (GET)
returns all todos

## Add
add - http://localhost:8080/api/v1/add?id=0&name=test&isdone=false (POST)
adds new todo

## Set
set - http://localhost:8080/api/v1/set?id=0&isdone=true (POST)
sets todo by id and takes in boolean 

## Remove
remove - http://localhost:8080/api/v1/remove?id=0 (POST)
removes todo by its id 
